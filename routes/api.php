<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::resource('cycle',\App\Http\Controllers\CycleController::class);
Route::resource('invoice',\App\Http\Controllers\InvoiceController::class);
Route::resource('invoiceitem',\App\Http\Controllers\InvoiceItemController::class);
Route::resource('module',\App\Http\Controllers\ModuleController::class);
Route::resource('order',\App\Http\Controllers\OrderController::class);
Route::resource('product',\App\Http\Controllers\ProductController::class);
Route::resource('service',\App\Http\Controllers\ServiceController::class);
Route::resource('user',\App\Http\Controllers\UserController::class);







<?php

namespace App\Handlers\ServiceHandlers;

abstract class AbstractHandler implements IHandler
{

    protected $next;
    private static $data = [];

    /**
     * @return array
     */
    public static function getData(): array
    {
        return self::$data;
    }

    /**
     * @param array $data
     */
    public static function setData(array $data): void
    {
        self::$data = $data;
    }

    public function setnext(IHandler $next)
    {
        $this->next = $next;
    }

    public function next($request = NULL)
    {
        if ($this->next) {
            return $this->next->handel($request);
        }
        var_dump('done');

    }


}

<?php

namespace App\Handlers\ServiceHandlers;

use App\Models\Order;
use App\Models\Service;
use App\Repositories\ServiceRepository;
use Carbon\Carbon;

class ServiceHandler extends AbstractHandler
{

    private ServiceRepository $repositService;
    private $totalamount;

    /**
     * @return mixed
     */
    public function getTotalamount()
    {
        return $this->totalamount;
    }

    /**
     * @param mixed $totalamount
     */
    public function setTotalamount($totalamount): void
    {
        $this->totalamount = $totalamount;
    }

    public function __construct()
    {
        $this->repositService = new ServiceRepository();
    }

    public function handel($request = null)
    {
      //  dd($request['product_ids']);
        $data['status'] = Order::STATUS_ACTIVE;
        $total_amount = 0;
        $repositService = $this->repositService;
        $service['services'] = collect($request['product_ids'])->map(function ($item, $key) use ($repositService, $request, &$total_amount) {
            $out = $repositService->create([
                'user_id' => $request['user_id'],
                'product_id' => $item,
                'cycle_id' => $request['cycle_ids'][$key],
                'started_at' => Carbon::now()->addDay(),
                'status' => Service::STATUS_ACTIVE,
            ]);

            $total_amount += ($out->product->cycle->where('id', $request['cycle_ids'][$key])->first())->amount;
            return $out;
        });

        $this->setTotalamount($total_amount);
        $service['totalAmount']=$total_amount;
        $service['request']=$request;
      //dd($service);
        AbstractHandler::setData($service);
        //return $this->next($service);

    }
}

<?php

namespace App\Handlers\ServiceHandlers;

use App\Models\Order;
use App\Repositories\InvoiceItemRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderRepository;
use Carbon\Carbon;

class OrderHandler
{

    private OrderRepository $repositOrder;

    public function __construct()
    {
        $this->repositOrder = new OrderRepository();
    }

    public function handel($request = null)
    {

        $order['order'] = $this->repositOrder->create([
            'user_id' => $request['request']['user_id'],
            'total_amount' => $request['totalAmount'],
            'status' => Order::STATUS_ACTIVE
        ]);
        $order +=$request;

        AbstractHandler::setData($order);

    }

    /**
     * @return InvoiceItemRepository
     */
    public function getRepositOrder(): InvoiceItemRepository
    {
        return $this->repositOrder;
    }

    /**
     * @param InvoiceItemRepository $repositOrder
     */
    public function setRepositOrder(InvoiceItemRepository $repositOrder): void
    {
        $this->repositOrder = $repositOrder;
    }
}

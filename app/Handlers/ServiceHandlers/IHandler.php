<?php

namespace App\Handlers\ServiceHandlers;

interface IHandler
{

    public function setnext(IHandler $next);

    public function handel($request = null);

    public function next($request = null);

}

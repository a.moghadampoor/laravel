<?php

namespace App\Handlers\ServiceHandlers;

use App\Models\Order;
use App\Repositories\InvoiceItemRepository;
use App\Repositories\InvoiceRepository;
use Carbon\Carbon;

class InvoiceHandler extends AbstractHandler
{
    private InvoiceRepository $repositInvoice;

    public function __construct()
    {
        $this->repositInvoice = new InvoiceRepository();
    }

    public function handel($request = null)
    {

        $invoices = $this->repositInvoice->create([
            'user_id' => $request['request']['user_id'],
            'total_amount' => $request['totalAmount'],
            'status' => Order::STATUS_ACTIVE,
            'expired_at' => Carbon::now()->addHour(),
        ]);

        $invoices += $request;

        AbstractHandler::setData((array)$invoices);

    }

    /**
     * @return InvoiceItemRepository
     */
    public function getRepositInvoice(): InvoiceItemRepository
    {
        return $this->repositInvoice;
    }

    /**
     * @param InvoiceItemRepository $repositInvoice
     */
    public function setRepositInvoice(InvoiceItemRepository $repositInvoice): void
    {
        $this->repositInvoice = $repositInvoice;
    }
}

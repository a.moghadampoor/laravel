<?php

namespace App\Repositories;


use App\Models\Order;

class OrderRepository
{
    protected $model;

    public function __construct()
    {
        $this->model=new Order();
    }
    public function index()
    {
        $this->model->all();
    }
    public function create($data)
    {

       return $this->model->create($data);
    }
    public function update($order , $data)
    {
        return tap($order)->update($data);
    }

    public function updateInvoiceId($order , $data)
    {
        return $this->model->where('id','=',$order)->update($data);
    }
    public function delete($id)
    {
        $this->model->whereId($id)->delete();
    }


}

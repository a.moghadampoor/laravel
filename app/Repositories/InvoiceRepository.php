<?php

namespace App\Repositories;


use App\Models\Invoice;
use App\Models\InvoiceItem;

class InvoiceRepository
{
    protected  $model;

    public function __construct()
    {
        $this->model=new Invoice();
    }
    public function index()
    {
       return $this->model->all();
    }
    public function create($data)
    {
      return  $this->model->create($data);
    }
    public function update($invoice , $data)
    {

        return tap($invoice)->update($data);
    }
    public function delete($id)
    {


        $this->model->whereId($id)->delete();
    }
}

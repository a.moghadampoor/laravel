<?php

namespace App\Repositories;


use App\Models\InvoiceItem;

class InvoiceItemRepository
{
    protected $model;

    public function __construct()
    {
        $this->model=new InvoiceItem();
    }
    public function index()
    {

    }
    public function create($data)
    {

     return   $this->model->create($data);

    }
    public function update()
    {

    }
    public function delete()
    {

    }
}

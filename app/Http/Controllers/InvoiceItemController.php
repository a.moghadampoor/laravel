<?php

namespace App\Http\Controllers;

use App\Models\InvoiceItem;
use App\Http\Requests\StoreInvoiceItemsRequest;
use App\Http\Requests\UpdateInvoiceItemsRequest;

class InvoiceItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInvoiceItemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInvoiceItemsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceItem  $invoiceItems
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceItem $invoiceItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvoiceItem  $invoiceItems
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceItem $invoiceItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInvoiceItemsRequest  $request
     * @param  \App\Models\InvoiceItem  $invoiceItems
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceItemsRequest $request, InvoiceItem $invoiceItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceItem  $invoiceItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceItem $invoiceItems)
    {
        //
    }
}

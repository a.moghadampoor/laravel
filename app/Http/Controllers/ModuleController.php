<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Http\Requests\StoreModulesRequest;
use App\Http\Requests\UpdateModulesRequest;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreModulesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreModulesRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Module  $modules
     * @return \Illuminate\Http\Response
     */
    public function show(Module $modules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $modules
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $modules)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateModulesRequest  $request
     * @param  \App\Models\Module  $modules
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateModulesRequest $request, Module $modules)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $modules
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $modules)
    {
        //
    }
}

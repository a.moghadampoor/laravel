<?php

namespace App\Http\Controllers;

use App\Handlers\ServiceHandlers\AbstractHandler;
use App\Handlers\ServiceHandlers\InvoiceHandler;
use App\Handlers\ServiceHandlers\OrderHandler;
use App\Handlers\ServiceHandlers\ServiceHandler;
use App\Models\Invoice;
use App\Models\Order;
use App\Http\Requests\StoreOrdersRequest;
use App\Http\Requests\UpdateOrdersRequest;
use App\Models\Service;
use App\Repositories\InvoiceItemRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreOrdersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $repositOreorder = new OrderRepository();
        $repositService = new ServiceRepository();
        $repositInvoice = new InvoiceRepository();
        $repositItemRepository = new InvoiceItemRepository();


        $data = $request->validate([
            'user_id' => 'required|integer',
            'product_ids' => 'required|array',
            'product_ids.*' => 'required|exists:products,id',
            'cycle_ids' => 'required|array',
            'cycle_ids.*' => 'required|exists:cycles,id',
        ]);

        AbstractHandler::setData($data);

        // dd(AbstractHandler::getData()) ;

        $serviceHandler = new ServiceHandler();
        $serviceHandler->handel(AbstractHandler::getData());


        $orderHandler = new OrderHandler();
        $orderHandler->handel(AbstractHandler::getData());




        $serviceHandler = new InvoiceHandler();
        $serviceHandler->handel(AbstractHandler::getData());
         dd(AbstractHandler::getData());



        //dd($order);

        $invoices = $repositInvoice->create([
            'user_id' => $data['user_id'],
            'total_amount' => $total_amount,
            'status' => Order::STATUS_ACTIVE,
            'expired_at' => Carbon::now()->addHour(),
        ]);


        $items = collect($service)->map(function ($item) use ($invoices) {
            return [
                'invoice_id' => $invoices['id'],
                'product_id' => $item->product_id,
                'cycle_id' => $item->cycle_id,
                'service_id' => $item->id,
                'amount' => ($item->product->cycle->where('id', $item->cycle_id)->first())->amount
            ];

        });

        $itemout = collect($items)->map(function ($item) use ($repositItemRepository) {

            return $repositItemRepository->create([
                'invoice_id' => $item['invoice_id'],
                'product_id' => $item['product_id'],
                'service_id' => $item['service_id'],
                'cycle_id' => $item['cycle_id'],
                'amount' => $item['amount'],

            ]);
        });


        $repositOreorder->updateInvoiceId($order->id, [
            'invoice_id' => $invoices->id
        ]);


        dd($itemout, $order->id);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Order $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Order $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Order $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateOrdersRequest $request
     * @param \App\Models\Order $orders
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrdersRequest $request, Order $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Order $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $orders)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cycle;
use App\Http\Requests\StoreCyclesRequest;
use App\Http\Requests\UpdateCyclesRequest;
use Illuminate\Http\Request;

class CycleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('dfddfdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCyclesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=$request->validate([
            'cycle'=>'required|string',
            'amount'=>'required|integer',
            'product_id'=>'required|integer',
        ]);
        dd($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cycle  $cycles
     * @return \Illuminate\Http\Response
     */
    public function show(Cycle $cycles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cycle  $cycles
     * @return \Illuminate\Http\Response
     */
    public function edit(Cycle $cycles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCyclesRequest  $request
     * @param  \App\Models\Cycle  $cycles
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCyclesRequest $request, Cycle $cycles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cycle  $cycles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cycle $cycles)
    {
        //
    }
}

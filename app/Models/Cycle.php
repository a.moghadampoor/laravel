<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    use HasFactory;

    protected $guarded=[];

    const CYCLE_ANNUALLY = 'annually';
    const CYCLE_HOURLY = 'hourly';
    const CYCLE_MONTHLY = 'monthly';


//    public function product()
//    {
//        return $this->belongsTo(Products::class);
//    }

}

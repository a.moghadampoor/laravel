<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    protected $guarded=[];

    public function Product()
    {
      return  $this->belongsTo(Product::class);
    }
}

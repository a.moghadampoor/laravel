<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function cycle()
    {
      return  $this->hasMany(Cycle::class);
    }

    public function service()
    {
      return  $this->belongsToMany(Service::class);
    }

}
